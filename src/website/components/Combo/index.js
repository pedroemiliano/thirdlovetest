import React, { Component } from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import styles from './styles.scss'

export default class Combo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null
    }
    this.handlerChange = this.handlerChange.bind(this)
  }

  handlerChange(e) {
    try {
      this.props.handlerChange(e)
      this.setState({
        selectedItem: e
      })
    } catch (error) {
      return null
    }
  }

  render() {
    return (
        <Select
          className="combo"
          placeholder="Select"
          onChange={this.handlerChange}
          value={this.state.selectedItem}
          searchable={false}
          clearable={false}
          options={this.props.data.map(size => ({ value: size, label: size }))}>             
        </Select>
    );
  }
}
