import React, { Component } from 'react'
import * as R from 'ramda'
import styles from './styles.scss'
import Swipeable from 'react-swipeable'
import ProductVariant from './../../containers/ProductVariant'

export default class Carousel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedIndex: 0,
            size: 0,
            marginLeft: 0
        }
        this.swipingLeft = this.swipingLeft.bind(this)
        this.image = React.createRef()
        this.size = 0
    }

    componentDidMount() {
        this.size = this.image.current.firstChild.clientWidth
        this.setState({
            selectedIndex: this.props.selectedIndex,
            size: this.size
        })
    }

    componentDidUpdate(prevProps) {
        if(prevProps.selectedIndex != this.props.selectedIndex) {
            console.log('prevProv', prevProps, 'props', this.props)
            this.setState({
                marginLeft: (this.props.selectedIndex * this.state.size)
            })
        }
    }

    imagePosition() {
        return {
            marginLeft: `-${this.state.marginLeft}px`
        }
    }

    swipingLeft(e, absX) {
        this.setState({
            marginLeft: absX
        })
    }

    render() {
        return (
            <div className="carousel">
            <Swipeable
                className="swipeable"
                onSwipingLeft={this.swipingLeft}>
                    <div className="content" ref={this.image} style={this.imagePosition()}>
                        {
                            this.props.data.map((image, i) => {
                                return <img key={i} src={image}/>
                            })
                        }
                    </div>    
            </Swipeable>
            </div>
        )
    }
}
