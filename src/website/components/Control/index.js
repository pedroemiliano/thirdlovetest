import React, { Component } from 'react'
import * as R from 'ramda'
import styles from './styles.scss'

const DEFAULT_GAP = 14

export default class Control extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedIndex: 0    
        }
    }

    getGap() {
        return {
            marginRight: `${R.defaultTo(DEFAULT_GAP)(this.props.gap)}px`
        }
    }

    handlerClick(index, item, e) {
        try {
            return this.props.handlerClick(index, item, e)
        } catch (error) {
            return null
        }
    }

    getControlClass() {
        let controlClass = 'control'
        if (this.props.className) {
            controlClass += ` ${this.props.className}`
        }
        return controlClass
    }

    getItemClasses(item, index) {
        let className = `dot dot-${index}`
        if (index === this.state.selectedIndex) {
            className += ' selected'
        }
        return className
    }

    render() {
        return (
            <div className={this.getControlClass()}>
                {
                    this.props.data.map((item, i) =>
                        <div 
                            key={i}
                            className={this.getItemClasses(item, i)}
                            onClick={(e) => {
                                this.handlerClick(i, item, e)
                                this.setState({selectedIndex : i})
                            }}
                        />
                    )
                }
            </div>
        )
    }
}
