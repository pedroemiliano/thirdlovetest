import React, { Component } from 'react';
import styles from './styles.scss'

export default class Thumbnail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0
    }
  }

  render() {
    return (
        <div className="thumbnails-container">
            {this.props.data.map((image, i) => 
              <img 
                key={i}
                src={image}
                className={(this.state.selectedIndex === i) ? 'selected' : ''}
                onClick={(e) => {
                  this.props.handler(i, e)
                  this.setState({
                    selectedIndex: i
                  })
                }}
              />
          )}
        </div>
    );
  }
}
