import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './containers/Home'
import styles from './styles/styles.scss'

const Index = () =>
  (<Router>
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          {/* <Route component={Home} /> */}
        </Switch>
      </div>
  </Router>)


ReactDOM.render(<Index />, document.getElementById("main-content"));