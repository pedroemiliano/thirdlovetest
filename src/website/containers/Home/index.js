import React, { Component } from 'react';
import styles from './styles.scss'
import * as R from 'ramda'
import Thumbnail from './../../components/Thumbnail'
import Carousel from './../../components/Carousel'
import Control from './../../components/Control'
import ProductVariant from './../../containers/ProductVariant'

const ENDPOINT = 'https://www.mocky.io/v2/5b3a92482e0000eb20158265'

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        title: null,
      },
      selectedIndex: 0
    }
    this.handlerThumbClick = this.handlerThumbClick.bind(this);
  }

  componentDidMount() {
    const { match : { params }} = this.props
    fetch(ENDPOINT)
      .then(res => res.json())
      .then((result) => {
        this.setState({ data: result.product })
      })
      .catch((err) => {
        console.log('error', err)
      });
  }

  handlerThumbClick(selectedIndex, item, e) {
    this.setState({selectedIndex})
  }

  text2html(text) {
    return text.replace(/(?:\r\n|\r|\n)/g, '<br/>')
  }

  createMarkup(html) {
    return {__html: html};
  }

  render() {
    if (!this.state.data.title) {
      return (
        <div>
          <div className="container">
            <div>Loading...</div>
          </div>
        </div>
      )
    }
    return (
      <div className="home">
        <h2 className="detail-title-top">{this.state.data.title}</h2>
        <h3 className="detail-price-top">{this.state.data.variants[0].price}</h3>
        <div className="image-detail">
          <div className="thumbnails">
              <Thumbnail
                data={R.pluck('src150')(this.state.data.images)}
                handler={this.handlerThumbClick}
              />
          </div>
          <div className="carousel-container">
            <Carousel data={R.pluck('srcMaster')(this.state.data.images)} selectedIndex={this.state.selectedIndex}/>
            <Control 
                data={this.state.data.images}
                // size={7}
                // gap={10}
                className="carousel-control"
                handlerClick={this.handlerThumbClick}
            />
          </div>
          <div className="detail">
              <h2 className="detail-title">{this.state.data.title}</h2>
              <ProductVariant data={this.state.data} />
          </div>
        </div>
        <div className="image-description">
          <h5>Details</h5>
          <p className="text" dangerouslySetInnerHTML={this.createMarkup(this.state.data.body_html)}></p>
        </div>
      </div>
    )
  }
}
