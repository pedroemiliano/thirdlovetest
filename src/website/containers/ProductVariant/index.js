import React, { Component } from 'react';
import styles from './styles.scss'
import * as R from 'ramda'
import Combo from './../../components/Combo'
import Control from './../../components/Control'
import Modal from 'react-responsive-modal';

export default class ProductVariant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProductVariant: this.props.data.variants[0],
      selectedBandSize: null,
      selectedCupSize: null,
      modalOpen: false
    }
    this.productvariantColors = R.pipe(R.pluck('option1'), R.uniq)(this.props.data.variants)
    this.onChangeBandSize = this.onChangeBandSize.bind(this)
    this.onChangeCupSize = this.onChangeCupSize.bind(this)
    this.handlerClick = this.handlerClick.bind(this)
    this.showBagItems = this.showBagItems.bind(this)
    this.onCloseModal = this.onCloseModal.bind(this)
  }

  handlerClick(i, variant, e) {
    e.preventDefault()
    this.setState({
      selectedProductVariant: R.find((item) => item.option1 === variant)(this.props.data.variants)
    })
  }

  onCloseModal() {
    this.setState({modalOpen: false})
  }

  showBagItems() {
    this.setState({modalOpen: true})
  }

  getSizeSplitted(size) {
    return R.reject(R.isEmpty, size.split(/(\d+)/g))
  }

  getBandSizeFromVariant(data) {
    const isOption1 = item => 
      item.option1 === this.state.selectedProductVariant.option1 &&
      item.inventory_quantity > 10
    const onlyNumbersRegex = /(\d+)/g;
    const getNumber = size => R.head(size.match(onlyNumbersRegex))
    return R.pipe(R.filter(isOption1), R.pluck('option2'), R.map(getNumber), R.uniq)(data)
  }

  getCupSizeFromVariant() {
    const onlyLetters = /[a-zA-Z()\s]+$/
    const hasColor = item => R.and(
      R.equals(item.option1,this.state.selectedProductVariant.option1),
      item.inventory_quantity > 10
    )
    const hasSize = item => R.contains(this.state.selectedBandSize, item.option2)
    const onlyLettersMap = item => R.head(item.match(onlyLetters))
    
    const result = R.pipe(
      R.filter(hasColor),
      R.filter(hasSize),
      R.pluck('option2'),
      R.map(onlyLettersMap),
      R.uniq
    )(this.props.data.variants)
    return result
  }

  onChangeBandSize(data) {
    this.setState({
      selectedBandSize: data.value
    })
  }

  onChangeCupSize(data) {
    this.setState({
      selectedCupSize: data.value
    })
  }

  render() {
    const { modalOpen } = this.state;
    return (
        <div className="product-variant-container">
            <div className="price">${this.state.selectedProductVariant.price}</div>
            <div className="product-variant-color">
              <span className="label">Color: </span>
              <span className="value">{this.state.selectedProductVariant.option1}</span>
            </div>
            <div>
                <Control
                  className="product-variant-control" 
                  data={this.productvariantColors}
                  handlerClick={this.handlerClick}
                  size={36}
                />
            </div>
            <div className="product-variant-stock">
              <span className="label">Stock: </span>
              <span className="value">{this.state.selectedProductVariant.inventory_quantity}</span>
            </div>
            
            <div className="size-container">
              <div className="band-size">
                <div className="band-size-label">
                  Band Size:
                </div>
                <div className="custom-select">
                  <Combo 
                    data={this.getBandSizeFromVariant(this.props.data.variants)}
                    handlerChange={this.onChangeBandSize}
                  />
                </div>
              </div>
              <div className="cup-size">
                <div className="cup-size-label">
                  Cup Size:
                </div>
                <Combo
                  data={this.getCupSizeFromVariant()}
                  handlerChange={this.onChangeCupSize}
                  />
              </div>            
            </div>
            <div>
              <button type="button" onClick = {this.showBagItems}>Add to bug</button>
            </div>
            <Modal
              open={modalOpen}
              onClose={this.onCloseModal}
              center
              classNames={{ overlay: 'custom-overlay', modal: 'custom-modal' }}>
              <h4>Added {this.props.data.title} - {this.state.selectedBandSize}{this.state.selectedCupSize} to the cart</h4>
            </Modal>
        </div>
    );
  }
}
