# ThirdLove Test

For this test I created This test was developed using those tecnologies:

* **npm** to handle all the packages and dependencies for this test
* **Node** 8 and babel to compile the test, in order to use ES6 features on any browser or devices
* **React**: I choose the **Container Pattern** because for me is the best way to separate the **data-fetching** and **rendering** concerns, only fetching the data from the containers and let the components be reusable and only for the presentation.
* CSS: CSS3 to stying the test for all browsers and devices (Media Queries), Flexbox to make the layout (could be choose css grid, but isn't supported for many browsers and devices yet)with **SASS** as a preprocessor
* **Webpack**: To build, package and minimize the test

## To run the Test

```
* npm run start
* The browser will be open with test http://localhost:3000/
```